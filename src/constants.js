// domain
// export const DOMAIN = "https://c43658a3a90b.ngrok.io";
// export const DOMAIN = "https://f32531d8ff9a.ngrok.io";
export const DOMAIN = "http://localhost:3002/";

export const LIMIT = 10;

// HTTP method 
export const HTTP_READ = "GET"
export const HTTP_CREATE = "POST"
export const HTTP_UPDATE = "PUT"
export const HTTP_DELETE = "DELETE"


export const GET_ITEM_REQUEST = 'GET_ITEM_REQUEST'
export const GET_ITEM_SUCCESS = 'GET_ITEM_SUCCESS'
export const GET_ITEM_FAILURE = 'GET_ITEM_FAILURE'
// HTTP headers
export const HTTP_HEADER_JSON = {"Content-Type": "Application/json"}

// types of actions
